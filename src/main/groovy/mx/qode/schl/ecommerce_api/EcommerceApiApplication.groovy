package mx.qode.schl.ecommerce_api

import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication

@SpringBootApplication
class EcommerceApiApplication {

    static void main(String[] args) {
        SpringApplication.run EcommerceApiApplication, args
    }
}
